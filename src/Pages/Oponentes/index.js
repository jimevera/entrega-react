import React from "react"
import { AuthContext } from "../../App"
import Oponente from "../../Components/Oponente/index"

//Assets
import './styles.css';
import logo from "../../Assets/LOGO.png";


const initialState = {
    oponentes: [],
    isFetching: false,
    hasError: false,
}

export const OponentesContext = React.createContext()

const reducer = (state, action) => {
    switch (action.type) {

        case "FETCH_OPONENTE_REQUEST":
            return {
                ...state,
                isFetching: true,
                hasError: false
            }

        case "FETCH_OPONENTE_SUCCESS":
            return {
                ...state,
                isFetching: false,
                oponentes: action.payload.oponentes
            }

        case "FETCH_OPONENTE_FAILURE":
            return {
                ...state,
                hasError: true,
                isFetching: false
            }

        default:
            return state
    }
}

export const Oponentes = () => {
    const { state: authState } = React.useContext(AuthContext)
    const [state, dispatch] = React.useReducer(reducer, initialState)
    
    React.useEffect(() => {
        if (authState.token) {
            dispatch({
                type: "FETCH_OPONENTE_REQUEST"
            })

            fetch(`${process.env.REACT_APP_API_PROTOCOL}://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}/oponentes`, {
                headers: {
                    "Authorization": authState.token,
                },

            }).then(response => {
                if (response.ok) {
                    return response.json()
                } else {
                    throw response
                }

            }).then(data => {
                dispatch({
                    type: "FETCH_OPONENTE_SUCCESS",
                    payload: data
                })

            }).catch(error => {
                dispatch({
                    type: "FETCH_OPONENTE_FAILURE"
                })
            })
        }
    }, [authState.token])
    
    return (
        <>
            <div className="home">
                <img src={logo} className="Logo" alt="logo" />
                <h1>Selecciona tu oponente:</h1>
            </div>
            <div>    
                {state.isFetching ? (
                    <span className="loader">Cargando...</span>
                ) : state.hasError ? (
                    <span className="error">Ocurrió un error</span>
                ) : (
                    <OponentesContext.Provider value={{ state, dispatch }}>
                    {state.oponentes.map(oponente => (
                        <Oponente key={oponente._id} oponente={oponente} />
                    ))}
                    </OponentesContext.Provider>
                )}
            </div>
        </>
    )
}

export default Oponentes