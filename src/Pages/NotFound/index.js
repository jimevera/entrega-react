// Dependencias
import React from "react";

// Assets
import "./styles.css";

const NotFound = () => <h1 className="NotFound">Página no encontrada</h1>;

export default NotFound;