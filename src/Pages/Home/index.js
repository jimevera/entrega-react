// Dependencias
import React from "react";
import { AuthContext } from "../../App";
import { Link } from "react-router-dom";
import ListGames from "../../Components/ListGames/index"

//Assets
import logo from "../../Assets/LOGO.png";
import "./styles.css";

export const Home = () => {
  const { state, dispatch } = React.useContext(AuthContext);
  
  return (
    <>
      <div className="Welcome">
        {state.isAuthenticated && (
          <nav>
            <h1>BIENVENIDO {state.user.email} ! </h1>
          </nav>
        )}
      </div>

      <div className="Contenedor">
        <nav className="PartidasJugadas">
          <h2>Partidas Jugadas </h2>
          <p>Usuario 1 vs Usuario 2</p>
          <Link to="/Ranking" className="App-button">Ranking</Link> 
        </nav>

        <nav className="Logout">
          <img src={logo} className="LogoHome" alt="logo" />
          {state.isAuthenticated && (
            <button
              className="App-button"
              onClick={() =>
                dispatch({
                  type: "LOGOUT",
                })
              }
            >
              Cerrar sesión
            </button>
          )}
        </nav>

        <nav className="PartidasEnCurso">
          <h2>Partidas en Curso </h2>
          <ListGames/>
          <Link to="/oponentes" className="App-button">Jugar!</Link> 
        </nav>
      </div>
    </>
  );
};

export default Home;
