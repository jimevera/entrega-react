import React from "react";
import { useHistory, useLocation } from "react-router-dom";
import { AuthContext } from "../../App";

//Assets
import estrella from "../../Assets/todos-estrella.png";
//import estrellita from "../../Assets/estrellita.png";
import estrellitavacia from "../../Assets/estrellitavacia.png";
import tijera from "../../Assets/tijeramano.png";
import papel from "../../Assets/papelmano.png";
import piedra from "../../Assets/piedramano.png";
import casa from "../../Assets/casa.png";
import papelgrande from "../../Assets/papel.png";
import piedragrande from "../../Assets/piedra.png";
import tijeragrande from "../../Assets/tijera.png";
import pregunta from "../../Assets/pregunta.png";
import './styles.css';

const Partida = (props) => {

    const location = useLocation();
    const history = useHistory();
    const { state, state: authState } = React.useContext(AuthContext);
    
    const onSubmit = () => { 
        history.push("/");
    }

/*     const inicializar = () => {
      if (state._id === location.state.id) {
      // el usuario location.state.id es el mio
        if (location.state.game.choices.length %2 !== 0) {
          //no es mi turno
          const choice = location.state.game.choices[location.state.game.choices.length -1]
          document.getElementById("papel").style.display="none"
          document.getElementById("tijera").style.display="none"
          document.getElementById("piedra").style.display="none"
          document.getElementById("papel2").style.display="none"
          document.getElementById("tijera2").style.display="none"
          document.getElementById("piedra2").style.display="none"
          document.getElementById("estrella").style.display="none"
          document.getElementById("pregunta").style.display="block"
          document.getElementById("mensaje").innerText="Esperando respuesta ..."
          if (choice === 1) {
            document.getElementById("piedragrande").style.display="block"
          }
          if (choice === 2) {
            document.getElementById("papelgrande").style.display="block"
          }
          if (choice === 3) {
          document.getElementById("tijeragrande").style.display="block"
          }

          if (choice === 1) {
            
          }
            else if (choice === 2) {

          } else{

          }
        }
      } else {
        //el usuario location.state.id es el oponente
      }
    }

    inicializar(); */

    const jugar = (choice) => {
      fetch(
        `${process.env.REACT_APP_API_PROTOCOL}://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}/games/choice/` + location.state.game._id + "/" + choice,
        {
          method: "put",
          headers: {
            Authorization: authState.token,
            "Content-Type": "application/json",
          },
          body: JSON.stringify(location.state.game),
        }
      )
      document.getElementById("papel").style.display="none"
      document.getElementById("tijera").style.display="none"
      document.getElementById("piedra").style.display="none"
      document.getElementById("papel2").style.display="none"
      document.getElementById("tijera2").style.display="none"
      document.getElementById("piedra2").style.display="none"
      document.getElementById("estrella").style.display="none"
      document.getElementById("pregunta").style.display="block"
      document.getElementById("mensaje").innerText="Esperando respuesta ..."
      if (choice === 1) {
        document.getElementById("piedragrande").style.display="block"
      }
      if (choice === 2) {
        document.getElementById("papelgrande").style.display="block"
      }
      if (choice === 3) {
      document.getElementById("tijeragrande").style.display="block"
      }
    }

    const hacerPiedra = () => {
      jugar(1)
    }

    const hacerPapel = () => {
      jugar(2)
    }

    const hacerTijera = () => {
      jugar(3)
    }

    return (
        <>
            <header className="header-game">
                {state.isAuthenticated && (
                    <nav className="usuario">
                        <h2>{state.user.email} </h2>
                        <img src={estrellitavacia} className="estrellitavacia" alt="estrellitavacia" />
                        <img src={estrellitavacia} className="estrellitavacia" alt="estrellitavacia" />
                        <img src={estrellitavacia} className="estrellitavacia" alt="estrellitavacia" />
                    </nav>   
                 )}
                 <img src={casa} className="casa" alt="casa" onClick={onSubmit}/>
                 
                <nav className="oponente">
                    <h2>Oponente email </h2>
                    <img src={estrellitavacia} className="estrellitavacia" alt="estrellitavacia" />
                    <img src={estrellitavacia} className="estrellitavacia" alt="estrellitavacia" />
                    <img src={estrellitavacia} className="estrellitavacia" alt="estrellitavacia" />
                </nav>   
                
            </header>

            <div className="centro">
                <div className="elementos">
                    <img src={tijera} onClick={hacerTijera} className="choice" alt="tijera" id="tijera"/>
                    <img src={papel} onClick={hacerPapel} className="choice" alt="papel" id="papel"/>
                    <img src={piedra} onClick={hacerPiedra} className="choice" alt="piedra" id="piedra"/>
                
                    <img src={tijeragrande} onClick={hacerTijera} className="choiceG hidden" alt="tijera" id="tijeragrande"/>
                    <img src={papelgrande} onClick={hacerPapel} className="choiceG hidden" alt="papel" id="papelgrande"/>
                    <img src={piedragrande} onClick={hacerPiedra} className="choiceG hidden" alt="piedra" id="piedragrande"/>
                
                </div>


                <div className="imagen">
                    <img src={estrella} className="estrella" alt="estrella" id="estrella"/>
                </div>

                <div className="elementos">
                    <img src={tijera} className="choice" alt="tijera" id="tijera2"/>
                    <img src={papel} className="choice" alt="papel" id="papel2"/>
                    <img src={piedra} className="choice" alt="piedra" id="piedra2"/>

                    <img src={pregunta} className="choiceG hidden" alt="pregunta" id="pregunta"/>
                </div>
            </div>    
            
            <div className="ronda">
                <h1 id="mensaje">Ronda 1</h1>
            </div>
        </>
    );
};
  
export default Partida;
