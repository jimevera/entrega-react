/* import { getRandomNumber } from '../helpers/utils'


import React from "react";

import "./styles.css";


import CardRow from '../UI/CardRow/CardRow'
import CardItem from '../UI/CardItem/CardItem'
import Button from '../UI/Button/Button'

import oponentCards from '../components/Computer/Computer'

import ic_piedra from '../images/ic_piedra.png'
import ic_papel from '../images/ic_papel.png'
import ic_tijera from '../images/ic_tijera.png'

export default class RPS extends React.Component {
    state = {
        cards: [
            {id: 1, choice: 'piedra', selected: false},
            {id: 2, choice: 'papel', selected: false},
            {id: 3, choice: 'tijera', selected: false}
        ],
        oponentCards: [
            {id: 1, choice: 'piedra', selected: false},
            {id: 2, choice: 'papel', selected: false},
            {id: 3, choice: 'tijera', selected: false}
        ],
        loading: false,
        combs: [
            {player: 'piedra', oponent: 'piedra', condition: 'empate'},
            {player: 'piedra', oponent: 'papel', condition: 'pierde'},
            {player: 'piedra', oponent: 'tijera', condition: 'gana'},
            {player: 'papel', oponent: 'piedra', condition: 'gana'},
            {player: 'papel', oponent: 'papel', condition: 'empate'},
            {player: 'papel', oponent: 'tijera', condition: 'pierde'},
            {player: 'tijera', oponent: 'piedra', condition: 'pierde'},
            {player: 'tijera', oponent: 'papel', condition: 'gana'},
            {player: 'tijera', oponent: 'tijera', condition: 'empate'}
        ],
        finished: null
    }

    selectedCardHandler = (choice) => {
        const updatedCardsElements = [...this.state.cards]
        const findIndex = updatedCardsElements.findIndex(v => v.choice === choice)
        updatedCardsElements[findIndex].selected = !updatedCardsElements[findIndex].selected
        this.letComputerPlay()
        this.setState({
            cards: updatedCardsElements,
            humanCardSelectedIndex: findIndex
        })
    }

    letOponentPlay = () => {
        this.setState({loading: true})
        setTimeout(() => {
            const updatedOponentOptions = [...this.state.oponentCards]
            const num = getRandomNumber(0, updatedComputerOptions.length)
            updatedOponentOptions[num].selected = !updatedOponentOptions[num].selected
            
            this.finishTheGame()

            this.setState({
                loading: false,
                oponentCards: updatedOponentOptions
            })
        }, 1000)
    }

    finishTheGame = () => {
        const combinations = [...this.state.combs]
        const playerSelected = this.state.cards.find(v => v.selected === true)
        const oponentSelected = this.state.oponentCards.find(v => v.selected === true)
        const combi = combinations.find(v => v.player === playerSelected.choice && v.oponent == oponentSelected.choice)
        
        let condition = ''
        if (combi.condition === 'empate') {
            condition = 'Empate'
        }
        if (combi.condition === 'gana') {
            condition = 'Ganaste'
        }
        if (combi.condition === 'pierde') {
            condition = 'Perdiste'
        }

        this.setState({finished: condition})
    }

    resetGameHandler = () => {
        let updatedCards = [...this.state.cards]
        let updatedOponentCards = [...this.state.oponentCards]
        updatedCards.forEach((v, i) => {
            updatedCards[i].selected = false
        })
        updatedComputeCards.forEach((v, i) => {
            updatedOponentCards[i].selected = false
        })
        this.setState({
            finished: null,
            cards: updatedCards,
            oponentCards: updatedOponentCards
        })
    }


    render() {
        const cards = this.state.cards
        const cardsOponent = this.state.oponentCards

        let hasSelectedOption = cards[0].selected || cards[1].selected || cards[2].selected
        let hasSelectedOponentOption = cardsOponent[0].selected || cardsOponent[1].selected || cardsOponent[2].selected


        return (
            <div className={classes.RPS}>
                <h2 style={{textAlign: 'center'}}>Elige una opcion</h2>
                <CardRow>
                    <CardItem 
                        selected={cards[0].selected}
                        cardAction={<Button disabled={hasSelectedOption} clicked={this.selectedCardHandler.bind(this, 'piedra')}>Piedra</Button>}>
                        <img src={ic_piedra} alt='piedra' title='Elige la piedra' />
                    </CardItem>
                    <CardItem
                        selected={cards[1].selected}
                        cardAction={<Button disabled={hasSelectedOption} clicked={this.selectedCardHandler.bind(this, 'papel')}>Papel</Button>}>
                        <img src={ic_papel} alt='papel' title='Elige el papel' />
                    </CardItem>
                    <CardItem
                        selected={cards[2].selected}
                        cardAction={<Button disabled={hasSelectedOption} clicked={this.selectedCardHandler.bind(this, 'tijera')}>Tijeras</Button>}>
                        <img src={ic_tijera} alt='tijera' title='Elige las tijeras' />
                    </CardItem>
                </CardRow>
                {this.state.loading ? <h3 style={{textAlign: 'center'}}>Pensando...</h3>: null}
                {hasSelectedComputerOption ? <oponentCards cards={cardsComputer} />: null}
                {
                    this.state.finished 
                        ? 
                            (
                                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>
                                    <h1 style={{textAlign: 'center', marginTop: '50px'}}>{this.state.finished}</h1> 
                                    <Button clicked={this.resetGameHandler}>Reset</Button>
                                </div>
                            )
                        : null
                }
            </div>
        )
    }
}
 */