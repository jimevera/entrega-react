import React from "react"
import { AuthContext } from "../../App"
import Card from "./Card"

export const RankingContext = React.createContext()

const initialState = {
    ranking: [],
    isFetching: false,
    hasError: false,
}

const reducer = (state, action) => {
    switch (action.type) {
        case "FETCH_RANKING_REQUEST":
            return {
                ...state,
                isFetching: true,
                hasError: false
            }
        case "FETCH_RANKING_SUCCESS":
            return {
                ...state,
                isFetching: false,
                ranking: action.payload.ranking
            }
        case "FETCH_RANKING_FAILURE":
            return {
                ...state,
                hasError: true,
                isFetching: false
            }
        
        default:
            return state
    }
}

export const Ranking = () => {
    const { state: authState } = React.useContext(AuthContext)
    const [state, dispatch] = React.useReducer(reducer, initialState)
    
    React.useEffect(() => {
        dispatch({
            type: "FETCH_RANKING_REQUEST"
        })

        fetch(`${process.env.REACT_APP_API_PROTOCOL}://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}/ranking`).then(response => {
            if (response.ok) {
                return response.json()
            } else {
                throw response
            }
        }).then(data => {
            dispatch({
                type: "FETCH_RANKING_SUCCESS",
                payload: data
            })
        }).catch(error => {
            dispatch({
                type: "FETCH_RANKING_FAILURE"
            })
        })
    }, [authState.token])

    return (
        <React.Fragment>
            <div className="home">

                {state.isFetching ? (
                    <span className="loader">Cargando...</span>
                ) : state.hasError ? (
                    <span className="error">Ocurrió un error</span>
                ) : (
                    <>
                    {
                        state.ranking.map(users => (
                            <Card key={users._id} users={users} />
                        ))
                    }
                    </>
                )}
            </div>
        </React.Fragment>
    )
}

export default Ranking



