// Dependencias
import React from "react";

import { AuthContext } from "../../App";

//Assets
import logo from "../../Assets/LOGO.png";
import "./styles.css";


export const Login = () => {
  const { dispatch } = React.useContext(AuthContext)

  const initialState = {
    email: "",
    password: "",
    isLoginSubmitting: false,
    isRegisterSubmitting: false,
    errorMessage: null,
  };

  const [data, setData] = React.useState(initialState)

  const handleInputChange = (event) => {
    setData({
      ...data,
      [event.target.name]: event.target.value
    });
  };

  const handleLoginSubmit = (event) => {
    event.preventDefault()

    setData({
      ...data,
      isLoginSubmitting: true,
      errorMessage: null,
    });

    fetch(
      `${process.env.REACT_APP_API_PROTOCOL}://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}/login`,
      {
        method: "post",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          email: data.email,
          password: data.password,
        }),
      }
    )
      .then((response) => {
        if (response.ok) {
          return response.json();
        }

        throw response;
      })
      .then((data) => {
        dispatch({
          type: "LOGIN",
          payload: data,
        });
      })
      .catch((error) => {
        setData({
          ...data,
          isLoginSubmitting: false,
          errorMessage: "Credenciales invalidas",
        });
      });
  };

  const handleRegisterSubmit = (event) => {
    event.preventDefault()

    setData({
      ...data,
      isRegisterSubmitting: true,
      errorMessage: null,
    });

    fetch(
      `${process.env.REACT_APP_API_PROTOCOL}://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}/register`,
      {
        method: "post",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          email: data.email,
          password: data.password,
        }),
      }
    )
      .then((response) => {
        if (response.ok) {
          return response.json();
        }

        throw response;
      })
      .then((data) => {
        dispatch({
          type: "LOGIN",
          payload: data,
        });
      })
      .catch((error) => {
        setData({
          ...data,
          isRegisterSubmitting: false,
          errorMessage: "No se pudo registar el usuario",
        });
      });
  };

  return (
    <div className="App">
      <header className="Header">
        <img src={logo} className="App-logo" alt="logo" />
        <form>
          <label htmlFor="email">
            Email
            <input
              type="text"
              value={data.email}
              onChange={handleInputChange}
              name="email"
              id="email"
            />
          </label>
          <label htmlFor="password">
            Contraseña
            <input
              type="password"
              value={data.password}
              onChange={handleInputChange}
              name="password"
              id="password"
            />
          </label>
          <button onClick={handleLoginSubmit} className="App-button" disabled={data.isLoginSubmitting}>
            {data.isLoginSubmitting ? "Iniciando sesion.." : "Iniciar sesion"}
          </button>
          <button onClick={handleRegisterSubmit} className="App-button" disabled={data.isRegisterSubmitting}>
            {data.isRegisterSubmitting ? "Registrando.." : "Registrate"}
          </button>
          {data.errorMessage && (
            <span className="form-error">{data.errorMessage}</span>
          )}
          
        </form>
      </header>
    </div>
  );
};

export default Login;
