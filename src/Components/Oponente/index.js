import React from "react";
import { useHistory } from "react-router-dom";

import { AuthContext } from "../../App";
import { OponentesContext } from "../../Pages/Oponentes"

//Assets
import './styles.css';

const MostrarOponentes = (props) => {

  const history = useHistory();
  const { dispatch } = React.useContext(OponentesContext);
  const { state: authState } = React.useContext(AuthContext);
  
  const onClose = (e) => {
    props.onClose && props.onClose(e);
    console.log("onclose")
  };
    
  const onSubmit = () => { 
    history.push("/partida");

    dispatch({
      type: "ADD_GAME_REQUEST",
    })
    
    console.log("hice click")
  ;
    
  const game = {
    idOponente: props.oponente._id
  }
    console.log("game");
    
    fetch(
      `${process.env.REACT_APP_API_PROTOCOL}://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}/games`,
      {
        method: "post",
        headers: {
          Authorization: authState.token,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(game),
      }
    )
    
      .then((response) => {
        console.log("fetch correcto")
        if (response.ok) {
          return response.json();
        } else {
          throw response;
        }
      })

      .then((data) => {
        dispatch({
          type: "ADD_GAME_SUCCESS",
          payload: data.game,
        });
        onClose();
      })

      .catch((error) => {
        dispatch({
          type: "ADD_GAME_FAILURE",
        });
      });
  };
  
  return (
    <div className="card">
      <button onClick={onSubmit} className="App-button-oponentes">{props.oponente.email}</button>
    </div>
  );
};

export default MostrarOponentes;