import React from "react"
import { AuthContext } from "../../App"
import Card from "../../Components/ListGames/card"


export const GameContext = React.createContext()

const initialState = {
    games: [],
    isFetching: false,
    hasError: false,
}

const reducer = (state, action) => {
    switch (action.type) {
        case "FETCH_GAMES_REQUEST":
            return {
                ...state,
                isFetching: true,
                hasError: false
            }
        case "FETCH_GAMES_SUCCESS":
            return {
                ...state,
                isFetching: false,
                games: action.payload.games
            }
        case "FETCH_GAMES_FAILURE":
            return {
                ...state,
                hasError: true,
                isFetching: false
            }
        default:
            return state
    }
}

export const Games = () => {
    const { state: authState } = React.useContext(AuthContext)
    const [state, dispatch] = React.useReducer(reducer, initialState)

        React.useEffect(() => {
        dispatch({
            type: "FETCH_GAMES_REQUEST"
        })

        fetch(`${process.env.REACT_APP_API_PROTOCOL}://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}/games`, {
            headers: {
                Authorization: authState.token,
                "Content-Type": "application/json",
            },
        })
        .then(response => {
            if (response.ok) {
                return response.json()
            } else {
                throw response
            }
        }).then(data => {
            dispatch({
                type: "FETCH_GAMES_SUCCESS",
                payload: data
            })
        }).catch(error => {
            dispatch({
                type: "FETCH_GAMES_FAILURE"
            })
        })
    }, [authState.token])

    return (
        <React.Fragment>
            <div className="games">
                {state.isFetching ? (
                    <span className="loader">Cargando...</span>
                ) : state.hasError ? (
                    <span className="error">Ocurrió un error</span>
                ) : (
                    <>
                    {state.games.map(game => (
                            <Card key={game._id} game={game} />
                        ))
                    }
                    </>
                )}
            </div>
        </React.Fragment>
    )
}

export default Games
