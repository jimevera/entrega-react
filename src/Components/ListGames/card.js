import React from "react"
import { useHistory } from "react-router-dom";

//Assets
import './styles.css';
import estrellitavacia from "../../Assets/estrellitavacia.png";

export const Card = ({ game }) => {
    const history = useHistory();
    
    const onSubmit = () => { 
        history.push({
            pathname: "/partida",
            state: {game: game}
        });
    }
    
    return (
        <div>
            <div className="partida">
                <p onClick={onSubmit}>{game.id} vs {game.idOponente}</p>
                <img src={estrellitavacia} className="estrellitavaciaHome" alt="estrellitavacia" />
                <img src={estrellitavacia} className="estrellitavaciaHome" alt="estrellitavacia" />
                <img src={estrellitavacia} className="estrellitavaciaHome" alt="estrellitavacia" />
            </div>
        </div>
    )
}

export default Card