//Dependencias
import React from 'react';
import ReactDOM from 'react-dom';

//Componentes
import App from './App';

//Utils
import * as serviceWorker from './serviceWorker';

//Assets
import './global.css';


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);


serviceWorker.unregister();
