import React from "react";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";

//Pages
import Login from "./Pages/Login/index.js";
import Home from "./Pages/Home/index.js";
import Oponentes from "./Pages/Oponentes/index";
import Partida from "./Pages/Partida/index";
import NotFound from "./Pages/NotFound/index";
import Ranking from "./Pages/Ranking/index";

export const AuthContext = React.createContext();

const initialState = {
  isAuthenticated: false,
  user: null,
  token: null,
};

const reducer = (state, action) => {
  switch (action.type) {
    case "LOGIN":
      localStorage.setItem("user", JSON.stringify(action.payload.user));
      localStorage.setItem("token", action.payload.user.token);

      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.user,
        token: action.payload.user.token,
      };
    case "LOGOUT":
      localStorage.clear();

      return {
        ...state,
        isAuthenticated: false,
        user: null,
        token: null,
      };
    default:
      return state;
  }
};

function App() {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  React.useEffect(() => {
    const user = JSON.parse(localStorage.getItem("user"));
    const token = localStorage.getItem("token");

    if (user && token) {
      dispatch({
        type: "LOGIN",
        payload: {
          user,
          token,
        },
      });
    }
  }, []);

  return (
    <AuthContext.Provider
      value={{
        state,
        dispatch,
      }}
    >
      <Router className="app">
        <Switch>
          <Route exact path="/">
            {state.isAuthenticated ? <Home /> : <Redirect to='/login' />}
          </Route>
          <Route path="/oponentes">
            {state.isAuthenticated ? <Oponentes /> : <Redirect to='/login' />}
          </Route>
          <Route path="/partida">
            {state.isAuthenticated ? <Partida /> : <Redirect to='/login' />}
          </Route>
          <Route path="/ranking">
           {state.isAuthenticated ? <Ranking /> : <Redirect to='/login' />}
          </Route>
          <Route path="/login">
            {state.isAuthenticated ? <Redirect to='/' /> : <Login />}
          </Route>
          <Route>
            <NotFound />
          </Route>
        </Switch>
      </Router>
    </AuthContext.Provider>
  );
}

export default App;
